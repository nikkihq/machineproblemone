import java.util.*

fun main() {
    val userInput = mutableListOf("gray", "pink", "red", "violet", "blue", "blue")
    val rainbow = listOf("red", "orange", "yellow", "green", "blue", "indigo", "violet")
    userInput.replaceAll(String::toLowerCase)

    var others = 0
    for (item in userInput.distinct()) {
        if(item !in rainbow){
            others +=1
        }
        if(item in rainbow){
            println("$item = ${Collections.frequency(userInput, item)}")
        }
    }
    println("others = $others")

}








