fun main(){
    val questions = listOf(
        "1). What is 5 + 10? a. 15 b. 20 c. None of the above",
        "2). What is 10/2? a. 6 b. 5 c. None of the above",
        "3). What is 10 - 2? a. 8 b. 9 c. 10",
        "4). What is 10 x 2? a. 30 b. 40 c. 20",
        "5). Which one is an even number? a. 4 b. 3 c. 4 + 3",
        "6). What is 2-0? a. 0 b. 3 c. None of the above",
        "7). What is 10 < 6? a. true b. false c. None of the above",
        "8). What is 10 + 10 > 19 + 19? a. true b. false c. None of the above",
        "9). What is 10 == 10/2 + 5? a. true b. false c. None of the above",
        "10). What is 5 > 4? a. true b. false c. None of the above",
    )
    var hashMap : HashMap<Int, String> = HashMap (10)
    hashMap.put(1, "a")
    hashMap.put(2, "b")
    hashMap.put(3, "a")
    hashMap.put(4, "c")
    hashMap.put(5, "a")
    hashMap.put(6, "c")
    hashMap.put(7, "b")
    hashMap.put(8, "b")
    hashMap.put(9, "a")
    hashMap.put(10, "a")

    var container = 1
    var numOfCorrect = 0
    var numOfIncorrect = 0


    while(true){
        println(questions[container-1])
        print("Enter Answer: ")
        val regEx = "[abc]".toRegex()
        val user= readLine()!!.toLowerCase()

        if(!user.matches(regEx)){
            continue
        }else{
            println(hashMap.get(container))
            if(hashMap.get(container) == user){
                container += 1
                numOfCorrect += 1
                println("Correct!")
            }
            else{
                container += 1
                numOfIncorrect += 1
                println("The answer is ${hashMap.get(container-1)}")
            }
        }
        if(container == 11) break
        println("")
    }
    println("Number of Correct answers: $numOfCorrect")
    println("Number of Incorrect answers: $numOfIncorrect")

}