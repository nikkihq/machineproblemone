import java.util.Collections.sort

fun main(){

        print("Enter a string parameter not more than 15 characters: ")
        val stringParam = readLine()!!.toLowerCase().toList()
        if (stringParam.size == 0){
            println("Enter something: No string inputted")
        }
        else if(stringParam.size <= 15){
            if(stringParam.size % 2 == 0){
                println("String ${stringParam.joinToString("")} has an even length: ${stringParam.size}")
                val str = stringParam.joinToString("")
                println("New string: ${str.reversed()}")
            }
            else{
                println("String ${stringParam.joinToString("")} has an odd length: ${stringParam.size}")
                println("New String: ${stringParam.sorted().joinToString("")}")

            }
        }
        else{
            println("Try Again!. Please enter a string not more than 15 characters")
        }
}