fun main(){
    val namePhone: MutableMap<String, String> = mutableMapOf(
        "rommel" to "1234567",
        "nikki" to "8910111",
        "angelic" to "2131415",
        "josh" to "7689012",
        "melen" to "7412641",
        "kate" to "4261468",
        "pom" to "8979223",
        "jel" to "6968424",
        "kris" to "112398",
        "ria" to "2161415"
    )
    print("Search for a name: ")
    val userInput = readLine()!!.toLowerCase()
    var condition = true

    if(namePhone.contains(userInput)){
        println("$userInput's phone number is: ${namePhone.get(userInput)}")
    }
    else{
        println("Not found")
        while (namePhone.size < 30 && condition == true) {
            println("Enter the details")
            print("Name: ")
            val userInput = readLine()!!.toLowerCase()
            print("Number: ")
            val num = readLine()!!
            namePhone.put(userInput, num)
            println(namePhone)
            println("Do you want to continue? Y OR N")
            val cont = readLine()!!
            if(cont == "N") condition = false
        }
    }


}